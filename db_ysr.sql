-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17 Jun 2017 pada 08.35
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ysr`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(20) NOT NULL,
  `admin_email` varchar(30) NOT NULL,
  `admin_username` varchar(30) NOT NULL,
  `admin_password` varchar(30) NOT NULL,
  `admin_phone` varchar(20) NOT NULL,
  `admin_company` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bike_class`
--

CREATE TABLE `bike_class` (
  `bikeclass_id` int(10) NOT NULL,
  `bikeclass_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bike_class`
--

INSERT INTO `bike_class` (`bikeclass_id`, `bikeclass_name`) VALUES
(1, 'R15'),
(2, 'R25'),
(3, 'Super Sport');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blood_type`
--

CREATE TABLE `blood_type` (
  `blood_type_id` int(10) NOT NULL,
  `blood_type_name` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `blood_type`
--

INSERT INTO `blood_type` (`blood_type_id`, `blood_type_name`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'O'),
(4, 'AB');

-- --------------------------------------------------------

--
-- Struktur dari tabel `extrapit_booking`
--

CREATE TABLE `extrapit_booking` (
  `extrapit_id` int(10) NOT NULL,
  `extrapit_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `extrapit_booking`
--

INSERT INTO `extrapit_booking` (`extrapit_id`, `extrapit_name`) VALUES
(1, 'YES'),
(2, 'NO');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gender`
--

CREATE TABLE `gender` (
  `gender_id` int(10) NOT NULL,
  `gender_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gender`
--

INSERT INTO `gender` (`gender_id`, `gender_name`) VALUES
(1, 'Perempuan'),
(2, 'Laki-laki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_balap`
--

CREATE TABLE `jadwal_balap` (
  `race_id` int(10) NOT NULL,
  `race_name` varchar(25) NOT NULL,
  `venue_race` varchar(30) NOT NULL,
  `class_race` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `manager`
--

CREATE TABLE `manager` (
  `manager_id` int(10) NOT NULL,
  `manager_name` varchar(30) NOT NULL,
  `manager_username` varchar(30) NOT NULL,
  `manager_password` varchar(255) NOT NULL,
  `manager_email` varchar(30) NOT NULL,
  `manager_phone` varchar(20) NOT NULL,
  `captcha` varchar(20) NOT NULL,
  `photo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `manager`
--

INSERT INTO `manager` (`manager_id`, `manager_name`, `manager_username`, `manager_password`, `manager_email`, `manager_phone`, `captcha`, `photo`) VALUES
(13, 'agung tri', 'rixi', 'R5eOdsx8puH4e5glUJ0yOuV9Kw9cE5p2ogLyZBN0P8gn8c+CGMqAUy0/tXeSzp73RQiLPq962ltk52TVccQdug==', 'agung@gmail.com', '089673180069', '4156106351', 'ui-zac.JPG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pit_booking`
--

CREATE TABLE `pit_booking` (
  `pit_id` int(10) NOT NULL,
  `pit_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pit_booking`
--

INSERT INTO `pit_booking` (`pit_id`, `pit_name`) VALUES
(1, 'YES'),
(2, 'NO');

-- --------------------------------------------------------

--
-- Struktur dari tabel `register`
--

CREATE TABLE `register` (
  `register_id` int(10) NOT NULL,
  `payment` varchar(20) NOT NULL,
  `approve_date` date NOT NULL,
  `approve_status` varchar(20) NOT NULL,
  `extrapit_id` int(10) NOT NULL,
  `pit_id` int(10) NOT NULL,
  `rider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rider`
--

CREATE TABLE `rider` (
  `rider_id` int(10) NOT NULL,
  `rider_name` varchar(30) NOT NULL,
  `place_of_birth` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `age` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `weight` int(10) NOT NULL,
  `address` varchar(30) NOT NULL,
  `rider_email` varchar(30) NOT NULL,
  `rider_phone` varchar(20) NOT NULL,
  `start_number` int(20) NOT NULL,
  `no_ktp` varchar(30) NOT NULL,
  `no_sim` varchar(30) NOT NULL,
  `no_kis` varchar(30) NOT NULL,
  `image_ktp` text NOT NULL,
  `image_sim` text NOT NULL,
  `image_kis` text NOT NULL,
  `manager_id` int(10) NOT NULL,
  `blood_type_id` int(10) NOT NULL,
  `race_id` int(10) NOT NULL,
  `gender_id` int(10) NOT NULL,
  `bikeclass_id` int(10) NOT NULL,
  `riderclass_id` int(10) NOT NULL,
  `team_id` int(10) NOT NULL,
  `registered` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rider_class`
--

CREATE TABLE `rider_class` (
  `riderclass_id` int(10) NOT NULL,
  `riderclass_name` varchar(25) NOT NULL,
  `bikeclass_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rider_class`
--

INSERT INTO `rider_class` (`riderclass_id`, `riderclass_name`, `bikeclass_id`) VALUES
(1, 'Super Sport', 1),
(2, 'PRO', 2),
(3, 'Com A', 2),
(4, 'Com B', 2),
(5, 'Com Pro', 2),
(6, 'PRO', 3),
(7, 'Com A', 3),
(8, 'Com B', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `team_id` int(10) NOT NULL,
  `team_name` varchar(30) NOT NULL,
  `team_domisili` varchar(30) NOT NULL,
  `manager_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`team_id`, `team_name`, `team_domisili`, `manager_id`) VALUES
(1, 'R Team', 'klaten', 13),
(4, 'Rixi Team', 'Klaten', 13),
(5, 'Rossi', 'Italia', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admin_username` (`admin_username`);

--
-- Indexes for table `bike_class`
--
ALTER TABLE `bike_class`
  ADD PRIMARY KEY (`bikeclass_id`);

--
-- Indexes for table `blood_type`
--
ALTER TABLE `blood_type`
  ADD PRIMARY KEY (`blood_type_id`);

--
-- Indexes for table `extrapit_booking`
--
ALTER TABLE `extrapit_booking`
  ADD PRIMARY KEY (`extrapit_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `jadwal_balap`
--
ALTER TABLE `jadwal_balap`
  ADD PRIMARY KEY (`race_id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`manager_id`);

--
-- Indexes for table `pit_booking`
--
ALTER TABLE `pit_booking`
  ADD PRIMARY KEY (`pit_id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`register_id`),
  ADD KEY `extrapit_id` (`extrapit_id`),
  ADD KEY `pit_id` (`pit_id`),
  ADD KEY `rider_id` (`rider_id`);

--
-- Indexes for table `rider`
--
ALTER TABLE `rider`
  ADD PRIMARY KEY (`rider_id`),
  ADD UNIQUE KEY `start_number` (`start_number`),
  ADD KEY `manager_id` (`manager_id`),
  ADD KEY `blood_type_id` (`blood_type_id`),
  ADD KEY `race_id` (`race_id`),
  ADD KEY `gender_id` (`gender_id`),
  ADD KEY `bikeclass_id` (`bikeclass_id`),
  ADD KEY `riderclass_id` (`riderclass_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `rider_class`
--
ALTER TABLE `rider_class`
  ADD PRIMARY KEY (`riderclass_id`),
  ADD KEY `bikeclass_id` (`bikeclass_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bike_class`
--
ALTER TABLE `bike_class`
  MODIFY `bikeclass_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blood_type`
--
ALTER TABLE `blood_type`
  MODIFY `blood_type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `extrapit_booking`
--
ALTER TABLE `extrapit_booking`
  MODIFY `extrapit_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `gender_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jadwal_balap`
--
ALTER TABLE `jadwal_balap`
  MODIFY `race_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manager`
--
ALTER TABLE `manager`
  MODIFY `manager_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pit_booking`
--
ALTER TABLE `pit_booking`
  MODIFY `pit_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `register_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rider`
--
ALTER TABLE `rider`
  MODIFY `rider_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rider_class`
--
ALTER TABLE `rider_class`
  MODIFY `riderclass_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `register`
--
ALTER TABLE `register`
  ADD CONSTRAINT `extrapit_id` FOREIGN KEY (`extrapit_id`) REFERENCES `extrapit_booking` (`extrapit_id`),
  ADD CONSTRAINT `pit_id` FOREIGN KEY (`pit_id`) REFERENCES `pit_booking` (`pit_id`),
  ADD CONSTRAINT `rider_id` FOREIGN KEY (`rider_id`) REFERENCES `rider` (`rider_id`);

--
-- Ketidakleluasaan untuk tabel `rider`
--
ALTER TABLE `rider`
  ADD CONSTRAINT `bikeclass_id` FOREIGN KEY (`bikeclass_id`) REFERENCES `bike_class` (`bikeclass_id`),
  ADD CONSTRAINT `blood_type_id` FOREIGN KEY (`blood_type_id`) REFERENCES `blood_type` (`blood_type_id`),
  ADD CONSTRAINT `gender_id` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
  ADD CONSTRAINT `manager_id` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`manager_id`),
  ADD CONSTRAINT `race_id` FOREIGN KEY (`race_id`) REFERENCES `jadwal_balap` (`race_id`),
  ADD CONSTRAINT `riderclass_id_rider` FOREIGN KEY (`riderclass_id`) REFERENCES `rider_class` (`riderclass_id`),
  ADD CONSTRAINT `team_id` FOREIGN KEY (`team_id`) REFERENCES `team` (`team_id`);

--
-- Ketidakleluasaan untuk tabel `rider_class`
--
ALTER TABLE `rider_class`
  ADD CONSTRAINT `bikeclass_id_riderclass` FOREIGN KEY (`bikeclass_id`) REFERENCES `bike_class` (`bikeclass_id`);

--
-- Ketidakleluasaan untuk tabel `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `manager_id_team` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`manager_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
