<?php $this->load->view('manager_home/v_header');?>
	<div id="login-page">
		<div class="container">
			<form class="form-login">
			<h2 class="form-login-heading">WELCOME</h2>
		        <div class="login-wrap">
					<div class="centered">
						<a href="<?php echo base_url('mgr/login') ?>"><button type="button" class="btn btn-info">Masuk</button></a>&nbsp;
						<a href="<?php echo base_url('mgr/register') ?>"><button type="button" class="btn btn-info">Daftar</button></a>
					</div>
		            <hr>
		            <div class="registration">
		                <a class="" href="#">
		                    <b>Baca Regulasi</b>
		                </a>|
						<a class="" href="#">
		                    <b>Download Regulasi</b>
		                </a>
		            </div>
		        </div>
		    </form>	  	
	  	</div>
	</div>

	<script src="<?php echo base_url('files/js/jquery.js')?>"></script>
    <script src="<?php echo base_url('files/js/bootstrap.min.js')?>"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
	<script type="text/javascript" src="<?php echo base_url('files/js/jquery.backstretch.min.js')?>"></script>
    <script>
        $.backstretch("files/img/login-bg.jpg", {speed: 500});
    </script>
  </body>
</html>
