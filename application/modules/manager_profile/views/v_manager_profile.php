<?php $this->load->view('manager_dashboard/v_header');?>
	<section id="main-content">
        <section class="wrapper">
			<h3><i class="fa fa-angle-right"></i><b> PROFILE MANAGER</b></h3>
			<hr>
			<div class="row mt">
				<div class="col-lg-12">
					<div class="col-lg-12 ds">
						<?php foreach ($profile as $key) :?>
						<img src="<?php echo base_url('files/img/profile/'.$key->photo)?>" class="img-rounded" width="150"><br>
						</br>
						<div class="col-lg-12 ds">
							<div>
								
								<table border=0>
									<thead>
										<tr>
											<td>Username &nbsp;&nbsp;</td>
											<td>&nbsp;&nbsp;: <?php echo $key->manager_username ?> </td>
										</tr>
										<tr>
											<td>Nama Manager</td>
											<td>&nbsp;&nbsp;: <?php echo $key->manager_name ?> </td>
										</tr>
										<tr>
											<td>Nomer Telefon</td>
											<td>&nbsp;&nbsp;: <?php echo $key->manager_phone ?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td>&nbsp;&nbsp;: <?php echo $key->manager_email ?></td>
										</tr>			
									</thead>
								</table>
								<br><button type="button" class="btn btn-round btn-info" data-toggle="modal" href="editProfile.php#myModal">Edit Profile</button>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
      	</section>
      <!--main content end-->
  	</section>
	<!-- Modal bagian untuk tambah user baru nanti-->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
		    <div class="modal-content">
		        <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <center><h4 class="modal-title">EDIT PROFIL MANAGER</h4></center>
		        </div>
		        <div class="modal-body">
		        	<?php echo form_open_multipart("mgr/edit-profile");?>
		            <p class="centered">Masukkan data diri anda</p>
		            <br>
					Upload Foto<input type="file" name="photo" placeholder="" autocomplete="off" class="form-control placeholder-no-fix">
					<br>
					<input type="hidden" name="manager_id" class="form-control">
					Username<input type="text" name="manager_username" class="form-control" value="<?php echo $key->manager_username ?>" autofocus disabled>
					<br>
					Password Baru<input type="password" name="manager_password" class="form-control" value="<?php echo $key->manager_password ?>">
					<br>
					Nama Manager<input type="text" name="manager_name" class="form-control" value="<?php echo $key->manager_name ?>">
					<br>
					Nomer Telefon<input type="number" name="manager_phone" class="form-control" value="<?php echo $key->manager_phone ?>">
					<br>
					Email<input type="text" name="manager_email" class="form-control" value="<?php echo $key->manager_email ?>">
					<label class="checkbox"></label>
					<button class="btn btn-theme btn-block" href="" type="submit"><i class="fa fa-lock"></i> UPDATE</button>
		        </div>
		        <div class="modal-footer">
		            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
	            </div>
		    </div>
		</div>
	</div>
<?php $this->load->view('manager_dashboard/v_footer');?>