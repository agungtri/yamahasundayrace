<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
	class Profile extends CI_Model{
		function __construct(){
	      	parent::__construct();
	    }

	    function get_profile(){
	    	$id_manager = $this->session->userdata('logged_in')['manager_id'];
			$this->db->select('*');
			$this->db->from('manager');
			$this->db->where('manager_id', $id_manager);
			$data = $this->db->get();
			return $data->result();
		}

		function edit_data($where,$table){
            return $this->db->get_where($table,$where);
        }
        
        function update_data($where,$data,$table){
	        $this->db->where($where);
	        $this->db->update($table,$data);
    	}
	}
?>