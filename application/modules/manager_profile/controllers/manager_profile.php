<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_profile extends CI_Controller {
		function __construct(){
	      	parent::__construct();
	      	$this->load->model('Profile');
	    }

		function index(){
			if(!empty($this->session->userdata('logged_in'))){
				$profile = $this->Profile->get_profile();
				$data['profile'] = $profile;
				$this->load->view('v_manager_profile', $data);
			}else{
				redirect('mgr/cek-login');
			}
		}

		function select_edit($manager_id){
	        $where = array('manager_id' => $manager_id);
	        $data['edit_profile'] = $this->Profile->edit_data($where,'manager')->result();
	        $this->load->view('v_manager_profile',$data);
    	}

		function action_edit_profile(){
			$nmfile = "file_".time();
			$config['upload_path'] = './files/img/profile'; 
			$config['allowed_types'] = 'jpg|png|jpeg|bmp';
			$config['max_size'] = '2048'; 
			$config['max_width']  = '1288';
			$config['max_height']  = '768';
			$config['file_name'] = $nmfile;
	 
			$this->upload->initialize($config);
			 
			if($_FILES['photo']['name']){
				if  ($this->upload->do_upload('photo')){
					$value = $this->upload->data();
					$id = $this->input->post('manager_id');
					$nama = $this->input->post('manager_name');
					$password = $this->input->post('manager_password');
					$phone = $this->input->post('manager_phone');
					$email = $this->input->post('manager_email');
					$data = array(
						'manager_name' => $nama,
						'manager_password' => $password,
						'manager_phone' => $phone,
						'manager_email' => $email,
						'photo' =>$value['file_name']
					);
					$where = array('manager_id' => $id);
			        $update = $this->Profile->update_data($where,$data,'manager');
					echo "<script>alert ('Update Profile Berhasil!');</script>";
					redirect('mgr/profile', 'refresh');
		 		}
		 	}
		}
	}
?>