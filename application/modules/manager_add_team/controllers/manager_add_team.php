<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_add_team extends CI_Controller { 
		function __construct(){
        	parent::__construct();  
            $this->load->model('Team');
            $this->load->model('manager_profile/Profile');
        }

		function index(){
            if(!empty($this->session->userdata('logged_in'))){
                $data['team'] = $this->Team-> view_team()->result();
                $profile = $this->Profile->get_profile();
                $data['profile'] = $profile;
                $this->load->view('v_manager_add_team', $data);
            }else{
                redirect('mgr/cek-login');
            }
		}

		function register_team(){
            $this->form_validation->set_rules('team_name','Nama','required');  
            $this->form_validation->set_rules('team_domisili','Kota Domisili','required');  
     
            if($this->form_validation->run() == FALSE) {  
                $this->load->view('v_manager_add_team');  
            }else{
                $team_name = $this->input->post('team_name');
                $team_domisili = $this->input->post('team_domisili');
                $id_team = $this->session->userdata('logged_in')['manager_id'];
                
                $value = array(
                    'team_name' => $team_name,
                    'team_domisili' => $team_domisili,
                    'manager_id' => $id_team
                );

                if($value){
                    $daftar = $this->Team->daftar_team($value);
                    echo "<script>alert ('Data team berhasi di inputkan!');</script>";
                    redirect('mgr/add-team', 'refresh');   
                }else{
                    echo "<script>alert ('Data team gagal di inputkan!');</script>";
                    redirect('mgr/add-team', 'refresh');
                }
            }  
        }
	}
?>