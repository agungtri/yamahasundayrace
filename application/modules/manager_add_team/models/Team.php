<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class Team extends CI_Model {  
        function __construct(){
        	parent::__construct();
        }

      	function daftar_team($value){
          $this->db->insert('team', $value);
      	}

      	function view_team(){
      		$id_manager = $this->session->userdata('logged_in')['manager_id'];
      		$this->db->select('*');
          $this->db->from('team');        
          $this->db->where('manager_id', $id_manager);
          return $this->db->get();
      	}
  	}
?>