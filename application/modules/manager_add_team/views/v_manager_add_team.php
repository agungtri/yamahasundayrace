<?php $this->load->view('manager_dashboard/v_header');?>
	<section id="main-content">
        <section class="wrapper">
		<h3><i class="fa fa-angle-right"></i><b> TAMBAH NAMA TEAM</b></h3>
		<hr>
			<div class="row mt">
				<div class="col-lg-12">
					<div class="col-lg-12 ds">
						<h4 class="mb"><i class="fa fa-angle-right"></i> Tambahkan Informasi Data Nama Team</h4>
						<?php echo form_open("mgr/daftar-team");?>
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Nama Team</label>
								<div class="col-sm-10">
									<input type="text" name="team_name" class="form-control">&nbsp;
									<?php echo form_error('team_name', '<div style="color:red">','</div>');?>
								</div>
								<label class="col-sm-2 col-sm-2 control-label">Kota Domisili Team</label>
								<div class="col-sm-10">
									<input type="text" name="team_domisili" class="form-control">&nbsp;
									<?php echo form_error('team_domisili', '<div style="color:red">','</div>');?>
								</div>
								<div class="col-lg-12">
									<div class="pull-right">
									<button type="submit" class="btn btn-round btn-info">SUBMIT</button>
								</div>
							</div>
						</div>
						<h4 class="mb"><i class="fa fa-angle-right"></i> Daftar Nama Team</h4>
						<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	display" width="100%">
							<thead>
								<tr>
									<th>Id Nama Team</th>
									<th>Nama Team</th>
									<th>Kota Domisili</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($team as $t) : ?>
								<tr>
									<td><?php echo $t->team_id ?></td>
									<td><?php echo $t->team_name ?></td>
									<td><?php echo $t->team_domisili ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
    	</section>
 	</section>

      <!--main content end-->
  </section>
<?php $this->load->view('manager_dashboard/v_footer');?>