<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_dashboard extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->model('Dashboard');
			$this->load->model('manager_profile/Profile');
		}

		function index(){
			if(!empty($this->session->userdata('logged_in'))){
				$dashboard = $this->Dashboard->get_data();
				$data['dashboard'] = $dashboard;
				$profile = $this->Profile->get_profile();
				$data['profile'] = $profile;
                $this->load->view('v_dashboard', $data);
			}else{
				redirect('mgr/cek-login');
			}
		}

		function logout(){
			$this->session->sess_destroy();
			redirect('mgr/login');
		}

	}
?>