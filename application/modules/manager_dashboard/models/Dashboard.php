<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
	class Dashboard extends CI_Model{
		function __construct(){
	      	parent::__construct();
	    }

	    function get_data(){
	    	$this->db->select('*');
			$this->db->from('bike_class');
			$this->db->join('rider_class', 'rider_class.riderclass_id = bike_class.bikeclass_id');
			$query = $this->db->get();
			return $query->result();
	    }

	    function get_all(){
			$this->db->select("*");
			$this->db->from("manager");
			$data = $this->db->get();
			return $data->result();
		}
	}
?>