    </section>
    
	<script src="<?php echo base_url('files/js/jquery.js')?>"></script>
    <script src="<?php echo base_url('files/js/jquery-1.8.3.min.js')?>"></script>
    <script src="<?php echo base_url('files/js/bootstrap.min.js')?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('files/js/jquery.dcjqaccordion.2.7.js')?>"></script>
    <script src="<?php echo base_url('files/js/jquery.scrollTo.min.js')?>"></script>
    <script src="<?php echo base_url('files/js/jquery.nicescroll.js')?>"></script>
    <script src="<?php echo base_url('files/js/jquery.sparkline.js')?>"></script>

    <!--common script for all pages-->
    <script src="<?php echo base_url('files/js/common-scripts.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('files/js/gritter/js/jquery.gritter.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('files/js/gritter-conf.js')?>"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('files/js/sparkline-chart.js')?>"></script>    
	<script src="<?php echo base_url('files/js/zabuto_calendar.js')?>"></script>

    <!--script for this table-->
   

    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>	
	
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
  </body>
</html>
