<?php $this->load->view('manager_dashboard/v_header');?>
	<section id="main-content">
          <section class="wrapper">
			<h3><i class="fa fa-angle-right"></i><b> NEWS</b></h3>
			<hr>
				<section id="unseen">
					<div class="col-lg-12 ds">
						<div class="col-lg-12">
							<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th>Bike Class</th>
										<th>Rider Class</th>
										<th class="numeric">Harga Normal</th>
										<th class="numeric">Harga Promo</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($dashboard as $data) : ?>
									<tr>
										<td><?php echo $data->bikeclass_name ?></td>
										<td><?php echo $data->riderclass_name ?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
                </section>
          </section>
      </section>
<?php $this->load->view('manager_dashboard/v_footer');?>