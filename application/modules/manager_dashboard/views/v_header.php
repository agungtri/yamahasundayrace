<!DOCTYPE html>
<html lang="en">
  <head>
    <title>YAMAHA SUNDAY RACE 2017</title>
    <link type="text/css" href="<?php echo base_url('files/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('files/css/bootstrap.css') ?>" rel="stylesheet">

    <!--external css-->
    <link href="<?php echo base_url('files/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('files/css/zabuto_calendar.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('files/js/gritter/css/jquery.gritter.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('files/lineicons/style.css') ?>"">    
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('files/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('files/css/style-responsive.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('files/js/chart-master/Chart.js')?>"></script>
  </head>
<body>
    <section id="container">
       <!--header start-->
        <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="<?php echo base_url('mgr/dashboard') ?>" class="logo"><b>YAMAHA SUNDAY RACE 2017</b></a>
            <!--logo end-->
        </header>
          <!--header end-->
          <!--sidebar start-->
        <aside>
            <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                <?php foreach ($profile as $key) :?>
                <p class="centered"><a href="<?php echo base_url('/mgr/profile') ?>"><img src="<?php echo base_url('files/img/profile/'.$key->photo)?>" class="img-circle" width="90"></a></p>
                <?php endforeach;?>
                <h5 class="centered"><?php echo $this->session->userdata('logged_in')['manager_username']; ?></h5>
                 
                  <li class="mt">
            
                    <a class="" href="<?php echo base_url('/mgr/dashboard') ?>"> 
                      <i class="fa fa-desktop"></i>
                      <span>Dashboard</span>
                    </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/profile') ?>">
                      <i class="fa fa-user"></i>
                      <span>Profile</span>
                   </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/add-team') ?>">
                      <i class="fa fa-group"></i>
                      <span>Tambah Nama Team</span>
                    </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/register-team') ?>">
                      <i class="fa fa-list"></i>
                      <span>Daftar Team</span>
                    </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/race') ?>">
                      <i class="fa fa-trophy"></i>
                      <span>Race</span>
                    </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/report') ?>">
                      <i class="fa fa-dashboard"></i>
                      <span>Report</span>
                    </a>
                  </li>
                  <li class="sub-menu">
                    <a class="" href="<?php echo base_url('/mgr/logout') ?>">
                      <i class="fa fa-sign-out"></i>
                      <span>Logout</span>
                    </a>
                  </li>
                </ul>
              <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
