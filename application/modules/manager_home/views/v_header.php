<!DOCTYPE html>
<html lang="en">
  <head>
    <title>YAMAHA SUNDAY RACE 2017</title>

    <!-- Bootstrap core CSS -->
  	<link href="<?php echo base_url('files/css/bootstrap.css') ?>" rel="stylesheet">

      <!--external css-->
  	<link href="<?php echo base_url('files/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('files/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('files/css/style-responsive.css')?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
