	<!-- js placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url('files/js/jquery.js')?>"></script>
    <script src="<?php echo base_url('files/js/bootstrap.min.js')?>"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
	<script type="text/javascript" src="<?php echo base_url('files/js/jquery.backstretch.min.js')?>"></script>
    <script>
        $.backstretch("../files/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
