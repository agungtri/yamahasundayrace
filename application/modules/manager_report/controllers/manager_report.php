<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_report extends CI_Controller {  
		function __construct(){
            parent::__construct();  
            //$this->load->model('Report');
            $this->load->model('manager_profile/Profile');
        }

		function index(){
			if(!empty($this->session->userdata('logged_in'))){
               $profile = $this->Profile->get_profile();
               $data['profile'] = $profile;
               $this->load->view('v_manager_report', $data);
			}else{
				redirect('mgr/cek-login');
			}
		}
	}
?>