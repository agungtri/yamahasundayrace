<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_login extends CI_Controller{
		function __construct(){
			parent::__construct();		
			$this->load->model('Login');
		}

		function index(){
			if(!empty($this->session->userdata('logged_in')))
                redirect('mgr/dashboard');
			if(!empty($this->input->post()))
            {
            	$this->form_validation->set_rules('manager_username', 'Username','required|trim');  
	            $this->form_validation->set_rules('manager_password', 'Password','required|trim');
	            if($this->form_validation->run() == FALSE) {  
	                $this->load->view('v_manager_login');  
	            }else{
	            	$username = $this->input->post('manager_username');
                    $password = $this->input->post('manager_password');
                    $check_auth = $this->Login->cek_login($username,$password);
                    if($check_auth)
                    {
                        redirect('mgr/dashboard');
                    }
                    else
                    {
                        redirect('mgr/login');
                    }
	            }
            }else{
            	redirect('mgr/login', 'refresh');
            }
		}

		function login(){
			$this->load->view('v_manager_login');
		}

	}
?>