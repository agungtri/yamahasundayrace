<?php $this->load->view('manager_home/v_header');?>
	<div id="login-page">
	  	<div class="container">
		   <?php echo form_open("mgr/cek-login", "class='form-login'");?>
		    <h2 class="form-login-heading">Login</h2>
		    <?php 
		    if(!empty($this->session->flashdata('failure')))
            {?>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $this->session->flashdata('failure'); ?>
                </div>
            <?php 
            }?>
		    <div class="login-wrap">
		        <input type="text" name="manager_username" class="form-control" placeholder="Username" autofocus value="<?php echo set_value('manager_username'); ?>">
		        <?php echo form_error('manager_username', '<div style="color:red">','</div>');?>
		        <?php
                if(!empty($this->session->flashdata('failure_username')))
                {?>
                    <div class="alert alert-danger">
                    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('failure_username'); ?>
                    </div>
                <?php 
                }
                ?>
		        <br>
		        <input type="password" name="manager_password" class="form-control" placeholder="Password" value="<?php echo set_value('manager_password'); ?>">
		        <?php echo form_error('manager_password', '<div style="color:red">','</div>');?>
		        <?php
                if(!empty($this->session->flashdata('failure_password')))
                {?>
                    <div class="alert alert-danger">
                    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('failure_password'); ?>
                    </div>
                <?php 
                }
                ?>
		        <label class="checkbox">
		            <span class="pull-right">
		              	<a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		            </span>
		        </label>
		        <button class="btn btn-theme btn-block" href="" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		        <hr>
		    </div>
		
		          <!-- Modal -->
		    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Forgot Password ?</h4>
		                </div>
		                <div class="modal-body">
		                    <p>Enter your e-mail address below to reset your password.</p>
		                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
		                </div>
		                <div class="modal-footer">
		                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                    <button class="btn btn-theme" type="button">Submit</button>
		                </div>
		            </div>
		        </div>
		    </div>
		          <!-- modal -->
	  	</div>
	</div>
<?php $this->load->view('manager_home/v_footer');?>