<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class Login extends CI_Model{
		function __construct(){
	      	parent::__construct();  
	    }

		function cek_login($usename,$password){		
			$this->db->select('*');
            $this->db->from('manager');        
            $this->db->where('manager_username', $usename);
            $query = $this->db->get();

            if($query->num_rows() > 0)
            {
               $user = $query->row(); //echo $user->user_status; exit;
                    if($this->encrypt->decode($user->manager_password) === $password)
                    {
                        //store user data into session  
                        $sess_array = array(
                            'manager_id' => $user->manager_id,
                            'manager_name' => $user->manager_name, 
                            'manager_username' => $user->manager_username,                        
                            'manager_email' => $user->manager_email
                        );
                        $this->session->set_userdata('logged_in',$sess_array);              
                        return true;                  
                    }
                    else
                    {
                        $this->session->set_flashdata('failure_password','Incorrect password.');
                        return false;
                    }
            }
            else
            {
                $this->session->set_flashdata('failure_username','Invalid username.');
                return false;
            }
        }	
	}
?>
