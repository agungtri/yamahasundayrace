<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_register extends CI_Controller {  
        function __construct(){
        	parent::__construct();  
            $this->load->model('Register');
        }

        function index(){
            $this->load->view('v_manager_register');
        }

        function aksi_register(){
            $this->form_validation->set_rules('manager_username', 'Username','required|trim');  
            $this->form_validation->set_rules('manager_password', 'Password','required|trim');  
            $this->form_validation->set_rules('manager_name','Name','required');  
            $this->form_validation->set_rules('manager_phone','Phone','required');  
            $this->form_validation->set_rules('manager_email','Email','required|trim|valid_email');
            $this->form_validation->set_rules('captcha','Capctha','required|trim'); 
            if($this->form_validation->run() == FALSE) {  
                $this->load->view('v_manager_register');  
            }else{
                //if($this->Mediatutorialcaptcha->check_captcha()==TRUE){
                    $username = $this->input->post('manager_username');
                    $p = $this->input->post('manager_password');
                    $name = $this->input->post('manager_name');
                    $phone = $this->input->post('manager_phone');
                    $email = $this->input->post('manager_email');
                    $password = $this->encrypt->encode($p);
                    $captcha = $this->input->post('captcha');


                    $value = array(
                    	'manager_username' => $username,
                        'manager_password' => $password,
                        'manager_name' => $name,
                        'manager_phone' => $phone,
                        'manager_email' => $email,
                        'captcha' => $captcha,
                        'photo' => 'profile_default.png'
                    );

                    if($value){
                        $daftar = $this->Register->daftar($value);
                        $data['body']  = "join success, silahkan login<br/>";
                        echo "<script>alert ('Terimakasih telah melakukan pendaftaran. Mohon untuk cek email untuk verifikasi akun YSR. Terimakasih!');</script>";
                        redirect('mgr/login', 'refresh');   
                    }else{
                        echo "<script>alert ('Pendaftaran Gagal !');</script>";
                        redirect('mgr/register', 'refresh');
                    }
                //}
            }  
        }
    }
?>  