<?php $this->load->view('manager_home/v_header');?>
    </head>
	<div id="login-page">
	  	<div class="container">
		    <?php echo form_open("mgr/add-register", "class='form-login'");?>
		    <h2 class="form-login-heading">sign up</h2>
		    <div class="login-wrap">
		        <input type="text" name="manager_username" class="form-control" placeholder="Username"  value="<?php echo set_value('manager_username'); ?>">
		        <?php echo form_error('manager_username', '<div style="color:red">','</div>');?>
		        <br>
		        <input type="password" name="manager_password" class="form-control" placeholder="Password" value="<?php echo set_value('manager_password'); ?>">
		        <?php echo form_error('manager_password', '<div style="color:red">','</div>'); ?>
				<br>
		        <input type="text" name="manager_name" class="form-control" placeholder="Nama Manager" value="<?php echo set_value('manager_name'); ?>">
		        <?php echo form_error('manager_name', '<div style="color:red">','</div>'); ?>
				<br>
		        <input type="number" name="manager_phone" class="form-control" placeholder="Nomer Telfon" value="<?php echo set_value('manager_phone'); ?>">
		        <?php echo form_error('manager_phone', '<div style="color:red">','</div>'); ?>
				<br>
		        <input type="text" name="manager_email" class="form-control" placeholder="Email" value="<?php echo set_value('manager_email'); ?>">
		        <?php echo form_error('manager_email', '<div style="color:red">','</div>'); ?>
		        <br>
		        <!-- <label>*Type the Captcha number below</label><br/>
	        	
		        <input type="text" name="captcha" class="form-control" placeholder="Cpatcha" value="<?php echo set_value('captcha'); ?>">
		        <?php echo form_error('captcha', '<div style="color:red">','</div>'); ?> -->
		        <br>
		        <label class="checkbox"></label>
		        <button class="btn btn-theme btn-block" href="" type="submit"><i class="fa fa-lock"></i> SIGN UP</button>
	            <hr>
	        </div>
	  	</div>
	</div>
<?php $this->load->view('manager_home/v_footer');?>