<?php $this->load->view('manager_dashboard/v_header');?>
	<section id="main-content">
        <section class="wrapper">
			<h3><i class="fa fa-angle-right"></i><b> DAFTAR BALAP YANG DIIKUTI</b></h3>
			<hr>
			<div class="row mt">
				<div class="col-lg-12">
					<div class="col-lg-12 ds">
						<h4 class="mb"><i class="fa fa-angle-right"></i> Nama Team	: Si Tepu</h4>
						<h4 class="mb"><i class="fa fa-angle-right"></i> Kota Domisili : Yogyakarta</h4>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>Name</th>
									<th>Race Name</th>
									<th>Bike Class</th>
									<th>Rider Class</th>
									<th>Payment Status</th>
									<th>Approved</th>
									<th>Details</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Fendi</td>
									<td>YSR Seri 2</td>
									<td>Community A</td>
									<td>Sport 1500 cc</td>
									<td></td>
									<td>No</td>
									<td><button type="button" class="btn btn-round btn-info" data-toggle="modal" href="adminRegister.php#myModal">Details</button></td>
								</tr>
								<tr>
									<td>Salith</td>
									<td>YSR Seri 2</td>
									<td>Community B</td>
									<td>Sport 1500 cc</td>
									<td></td>
									<td>No</td>
									<td><button type="button" class="btn btn-round btn-info" data-toggle="modal" href="adminRegister.php#myModal">Details</button></td>
								</tr>
							</tbody>
						</table>			
					</div>
				</div>
			</div>
        </section>
    </section>  
	  <!-- Modal bagian untuk tambah user baru nanti-->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	 	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Invoice</h4>
	            </div>
	            <div class="modal-body">
		            <p class="centered"></p>
					<div class="row">
						<div class="col-md-3">
							<p>Register id</p>
							<p>Race id</p>
							<p>Nama</p>
							<p>Bike Class</p>
							<p>Rider Class</p>
							<p>Race Name</p>
							<p>Race Venue</p>
							<p>Start Number</p>
							<p>Payment Amount</p>
							<p>Pit</p>
							<p>Extra Pit</p>
							<p>Register Date</p>
							<p>Approve Date</p>
							<p>Status</p>
						</div>
						<div class="col-md-6">
							<p>: 285</p>
							<p>: SunRace2017_1</p>
							<p>: Fendi</p>
							<p>: Community A</p>
							<p>: Sport 1500 cc</p>
							<p>: YSR seri 2</p>
							<p>: Sentul, 15-16 Juli 2017</p>
							<p>: 001</p>
							<p>: </p>
							<p>: </p>
							<p>: </p>
							<p>: 2017-04-27</p>
							<p>: </p>
							<p>: Active</p>
						</div>
					</div>
	            </div>
	            <div class="modal-footer">
	                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
	            </div>
	        </div>
	    </div>
	</div>
	  <!-- akhir modal bagian untuk tambah user baru nanti -->
      <!--main content end-->
<?php $this->load->view('manager_dashboard/v_footer');?>