<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class manager_race extends CI_Controller {
		function __construct(){
            parent::__construct();  
            //$this->load->model('Race');
            $this->load->model('manager_profile/Profile');
        }

		function index(){
			if(!empty($this->session->userdata('logged_in'))){
               $profile = $this->Profile->get_profile();
               $data['profile'] = $profile;
               $this->load->view('v_manager_race', $data);
			}else{
				redirect('mgr/cek-login');
			}
		}
	}
?>