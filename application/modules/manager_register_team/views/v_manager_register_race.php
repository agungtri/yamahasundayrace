<?php $this->load->view('manager_dashboard/v_header');?>\
	<section id="main-content">
        <section class="wrapper">
			<h3><i class="fa fa-angle-right"></i><b> DAFTAR BALAP AKTIF</b></h3>
			<hr>
			<div class="row mt">
				<div class="col-lg-12">
					<div class="col-lg-12 ds">
						<h4 class="mb"><i class="fa fa-angle-right"></i> Pilih data balap yang anda ikuti</h4>
						<div class="mb">	
							<div class="col-md-2">									<img src="<?php echo base_url('files/img/ui-zac.jpg')?>" class="img-rounded" width="120"><br><br>
							</div>
							<div class="col-md-5">
								<p>Nama : Fandi</p>
								<p>Nama Tim : Si Tepu</p>
								<p>Domisili Tim : Yogyakarta</p>
							</div>
						</div>
						&nbsp;
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>Race Name</th>
									<th>Race Venue</th>
									<th>Race Class</th>
									<th>Register</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>YSR Seri 2</td>
									<td>sentul 15-16 Juli 2017</td>
									<td>All Class</td>
									<td><button type="button" class="btn btn-round btn-info">Register</button></td>
								</tr>
								<tr>
									<td>YSR Seri 2</td>
									<td>sentul 15-16 Juli 2017</td>
									<td>All Class</td>
									<td><button type="button" class="btn btn-round btn-info">Register</button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </section>
    </section>
<?php $this->load->view('manager_dashboard/v_footer');?>