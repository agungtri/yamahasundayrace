<?php $this->load->view('manager_dashboard/v_header');?>
	<section id="main-content">
        <section class="wrapper">
			<h3><i class="fa fa-angle-right"></i><b> MANAGE TEAM MU</b></h3>
			<hr>
			<div class="row mt">
				<div class="col-lg-12">
					<div class="col-lg-12 ds">
						<h4 class="mb"><i class="fa fa-angle-right"></i> Manage Team Mu</h4>
						<form class="form-horizontal style-form" method="get">
							<div class="form-group">
								<label class="col-sm-2 col-sm-2 control-label">Nama Team</label>
								<div>
									<?php
										echo "
										<select name='team' id='select-team' required>
										 <option value='' disabled selected>Pilih Team</option>";
										  foreach ($team as $row_team) {  
										  echo "<option value='".$row_team->team_id."'>".$row_team->team_name."</option>";
										  }
										  echo"
										</select>";
									?>
								</div>
								<label class="col-sm-2 col-sm-2 control-label">Kota Domisili</label>
								<div>
									<?php
										echo "
										<select name='team' id='select-team' required>
										 <option value='' disabled selected>Pilih Team</option>";
										  foreach ($team as $row_team) {  
										  echo "<option value='".$row_team->team_id."'>".$row_team->team_domisili."</option>";
										  }
										  echo"
										</select>";
									?>
								</div>
								
							</div>
						</form>
						<h4 class="mb"><i class="fa fa-angle-right"></i> Daftar Anggota Team</h4>
						<div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<?php foreach($rider as $r) :?>
							<div class="white-panel pn">
								<div class="centered">
									<div class="white-header">
										<h5>ANGGOTA</h5>
									</div>
									<img src="<?php echo base_url('files/img/profile/ui-zac.jpg')?>" class="img-circle" width="80"><br><br>
									<b>Zac Snider</b><br></br>
									<div class="row">
										<div class="col-md-6">
											<button type="button" class="btn btn-round btn-info" data-toggle="modal" href="adminRegister.php#ModalEditUser">Edit Profile</button>
										</div>
										<div class="col-md-6">
											<a href="<?php echo base_url('/mgr/register-race') ?>">
												<button type="button" class="btn btn-round btn-info" >Daftar Balap</button>
											</a>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach;?>
						</div><!-- /col-md-4 -->

							<!-- bagian yang tambah user-->
						<div class="col-lg-4 mb" data-toggle="modal" href="adminRegister.php#myModal">
							<div class="white-panel pn">
								<div class="white-header">
									<h5>Add Anggota</h5>
								</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
									</div>
									<div class="col-sm-6 col-xs-6"></div>
								</div>
								<div class="centered">
									<p><img src="<?php echo base_url('files/img/icon_1.png')?>" width="120"></p>
									<p><b>Add New</b></p>
								</div>
							</div>
						</div><!-- col-lg-4 --><!-- akhir bagian yang tambah user-->
					</div>
				</div>
			</div>
    	</section>
    </section>
	  
	  <!-- Modal bagian untuk tambah user baru nanti-->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
		    <div class="modal-content">
		        <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <center><h4 class="modal-title">ADD MEMBER TEAM</h4></center>
		        </div>
		        <div class="modal-body">
		            <p class="centered">Tambahkan informasi data diri member</p>
					<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
	                <input type="text" name="name" placeholder="Full Name" autocomplete="off" class="form-control placeholder-no-fix"><br>
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="place" placeholder="Place of Birth" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="number" name="age" placeholder="Age" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Height" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Blood Type" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
						<div class="col-md-6">
							<input type="date" name="email" placeholder="Date of Birth" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Gender" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Weight" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
					</div>
					<input type="password" name="email" placeholder="Address" autocomplete="off" class="form-control placeholder-no-fix"><br>
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Start Number" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="Bike Class" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="No. KTP" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="No. SIM" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="email" placeholder="No. KIS" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
						<div class="col-md-6">
							<input type="text" name="email" placeholder="Phone" autocomplete="off" class="form-control placeholder-no-fix"><br></br>&nbsp;<br></br>
							<input type="password" name="email" placeholder="Reder Class" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
					</div>
		        </div>
		        <div class="modal-footer">
		            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		            <button class="btn btn-theme" type="button">Submit</button>
		        </div>
		    </div>
		</div>
	</div>
	  <!-- akhir modal bagian untuk tambah user baru nanti -->
	  
	  <!-- Modal bagian untuk edit user-->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="ModalEditUser" class="modal fade">
        <div class="modal-dialog">
		    <div class="modal-content">
		        <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <center><h4 class="modal-title">EDIT MEMBER TEAM</h4></center>
		        </div>
		        <div class="modal-body">
		            <p class="centered">Tambahkan informasi data diri member</p>
					<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
		            <input type="text" name="name" placeholder="Full Name" autocomplete="off" class="form-control placeholder-no-fix"><br>
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="tempatLahir" placeholder="Place of Birth" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="umur" placeholder="Age" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="heigh" placeholder="Height" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="golonganDarah" placeholder="Blood Type" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
						<div class="col-md-6">
							<input type="text" name="tanggalLahir" placeholder="Date of Birth" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="gender" placeholder="Gender" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="weight" placeholder="Weight" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
					</div>
					<input type="password" name="address" placeholder="Address" autocomplete="off" class="form-control placeholder-no-fix"><br>
					<div class="row">
						<div class="col-md-6">
							<input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="startNumber" placeholder="Start Number" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="bikeClass" placeholder="Bike Class" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="noKTP" placeholder="No. KTP" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="noSIM" placeholder="No. SIM" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="password" name="noKIS" placeholder="No. KIS" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
						<div class="col-md-6">
							<input type="text" name="email" placeholder="Phone" autocomplete="off" class="form-control placeholder-no-fix"><br></br>&nbsp;<br></br>
							<input type="password" name="email" placeholder="Reder Class" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
							<input type="file" name="email" placeholder="" autocomplete="off" class="form-control placeholder-no-fix"><br>
						</div>
					</div>
		        </div>
		        <div class="modal-footer">
		            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		            <button class="btn btn-theme" type="button">Submit</button>
		        </div>
		    </div>
		</div>
	</div>
	  <!-- akhir modal bagian untuk edit user -->

<?php $this->load->view('manager_dashboard/v_footer');?>