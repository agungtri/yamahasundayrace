<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

	class Rider extends CI_Model {  
        function __construct(){
        	parent::__construct();
        }

        function get(){
          return $this->db->get("team");
        }

      	function daftar_rider($value){
          $this->db->insert('rider', $value);
      	}

      	function view_rider(){
      		$id_manager = $this->session->userdata('logged_in')['manager_id'];
      		$this->db->select('*');
          $this->db->from('rider');        
          $this->db->where('rider_id', $id_manager);
          return $this->db->get();
      	}
  	}
?>