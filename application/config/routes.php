<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'manager_index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//login dan register manager
$route['mgr/login'] = 'manager_login/login';
$route['mgr/cek-login'] = 'manager_login';
$route['mgr/register'] = 'manager_register';
$route['mgr/add-register'] = 'manager_register/aksi_register';

//menu dashboard
$route['mgr/dashboard'] = 'manager_dashboard';
$route['mgr/profile'] = 'manager_profile';
$route['mgr/add-team'] = 'manager_add_team';
$route['mgr/register-team'] = 'manager_register_team';
$route['mgr/race'] = 'manager_race';
$route['mgr/report'] = 'manager_report';

//profile
$route['mgr/profile'] = 'manager_profile';
$route['mgr/edit-profile'] = 'manager_profile/action_edit_profile';

//add_team
$route['mgr/daftar-team'] = 'manager_add_team/register_team';

//daftar team
$route['mgr/register-race'] = 'manager_register_team/register_race';

//logout
$route['mgr/logout'] = 'manager_dashboard/logout';